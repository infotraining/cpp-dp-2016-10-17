#include "document.hpp"
#include "command.hpp"
#include "application.hpp"

using namespace std;

class DocApplication : public Application
{
    Document doc_;
    CommandTracker cmd_tracker_;
public:
    DocApplication() : doc_{"Wzorzec projektowy: Command"}
    {
        // obiekty polecen
        unique_ptr<Command> cmd_paste(new PasteCmd(doc_, cmd_tracker_));
        unique_ptr<Command> cmd_undo(new UndoCmd(doc_, cmd_tracker_));
        unique_ptr<Command> cmd_print(new PrintCmd(doc_));
        unique_ptr<Command> cmd_toupper(new ToUpperCmd(doc_, cmd_tracker_));
        // TODO: CopyCmd
        // TODO: ToLowerCmd
        // TODO: AddTextCmd

        // przyciski
        auto menu_paste = MenuItem {"Paste", move(cmd_paste)};
        auto menu_toupper = MenuItem {"ToUpper", move(cmd_toupper)};
        auto menu_undo = MenuItem {"Undo", move(cmd_undo)};
        auto menu_print = MenuItem {"Print", move(cmd_print)};
        // TODO: menu_copy
        // TODO: menu_tolower
        // TODO: menu_add

        add_menu(move(menu_paste));
        add_menu(move(menu_toupper));
        add_menu(move(menu_print));
        add_menu(move(menu_undo));
        // TODO: menu_copy
        // TODO: menu_tolower
        // TODO: menu_add
    }
};

int main()
{
    DocApplication app;

    const std::string exit_application = "end";

	std::string cmd;
	do
	{
		std::cout << "Enter command: ";
		std::cin >> cmd;
		app.execute_action(cmd);
    } while (cmd != exit_application);

	std::cout << "End of application..." << std::endl;

    return 0;
}

