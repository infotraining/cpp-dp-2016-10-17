#include "stock.hpp"

using namespace std;

int main()
{
	Stock misys("Misys", 340.0);
	Stock ibm("IBM", 245.0);
	Stock tpsa("TPSA", 95.0);

	// rejestracja inwestor�w zainteresowanych powiadomieniami o zmianach kursu sp�ek
    Investor kulczyk_jr{"KulczykJr"};
    Investor solorz{"Solorz"};

    tpsa.attach(&solorz);
    tpsa.attach(&kulczyk_jr);
    ibm.attach(&solorz);


	// zmian kurs�w
	misys.set_price(360.0);
	ibm.set_price(210.0);
	tpsa.set_price(45.0);

    cout << "\n\n";
    tpsa.detach(&solorz);


	misys.set_price(380.0);
	ibm.set_price(230.0);
	tpsa.set_price(15.0);
}
