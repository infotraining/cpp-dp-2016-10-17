#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <string>
#include <iostream>
#include <set>

class Observer
{
public:
    virtual void update(std::string symbol, double price) = 0;
    virtual ~Observer() = default;
};

// Subject
class Stock
{
private:
    std::string symbol_;
    double price_;
    std::set<Observer*> observers_;
public:
    Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
    {
    }

    std::string get_symbol() const
    {
        return symbol_;
    }

    double get_price() const
    {
        return price_;
    }

    // rejestracja obserwatora
    void attach(Observer* o)
    {
        observers_.insert(o);
    }

    // wyrejestrowanie obserwatora
    void detach(Observer* o)
    {
        observers_.erase(o);
    }

    void set_price(double price)
    {
        if (price != price_)
        {
            price_ = price;

            // powiadomienie inwestorow o zmianie kursu...
            notify();
        }

    }
protected:
    void notify()
    {
        for(const auto& o : observers_)
            o->update(symbol_, price_);
    }
};


class Investor : public Observer
{
    std::string name_;
public:
    Investor(const std::string& name) : name_(name)
    {
    }

    void update(std::string symbol, double price) override
    {
        std::cout << name_ << " is notified "
             << " { " << symbol << ", " << price << "$ }" << std::endl;
    }
};

#endif /*STOCK_HPP_*/
