get_filename_component(ProjectId ${CMAKE_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
project(${ProjectId})

cmake_minimum_required(VERSION 2.8)

aux_source_directory(. SRC_LIST)

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
	# using Clang
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  	# using GCC
  	add_definitions("-std=c++14")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
  	# using Intel C++
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
  	# using Visual Studio C++
endif()

file(GLOB header_FILES "*.h" "*.hpp")
add_executable(${PROJECT_NAME} ${SRC_LIST} ${header_FILES})