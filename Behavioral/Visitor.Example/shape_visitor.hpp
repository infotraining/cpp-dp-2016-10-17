#pragma once

class Circle;
class Rectangle;

struct ShapeVisitor
{
    virtual void visit(Circle& c) = 0;
    virtual void visit(Rectangle& r) = 0;

    virtual ~ShapeVisitor() = default;
};
