#include <iostream>
#include <functional>

using namespace std;

int foo(string name)
{
    cout << "foo(string: " << name << ")\n";

    return name.length();
}

class Foo
{
public:
    int operator()(string name)
    {
        cout << "Foo::operator()(string: " << name << ")\n";

        return name.length();
    }
};


int main(int argc, char *argv[])
{
    int (*ptr_fun)(string) = &foo;
    ptr_fun("hello");

    Foo foonctor;
    foonctor("hello");

    auto lambda =
            [](string name) { cout << "lambda(string: " << name <<")\n"; return name.length(); };
    lambda("hello");

    function<int(string)> f;
    f = &foo;
    f("hello with std::function");

    f = foonctor;
    f("hello with std::function");

    f = [](string name) { cout << "lambda(string: " << name <<")\n"; return name.length();};
    f("hello with std::function");
}
