#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>
#include <memory>

using namespace std;

#define WINDOWS

class Widget
{
public:
    virtual void draw() = 0;
    virtual ~Widget() = default;
};

class MotifButton : public Widget
{
public:
    void draw() override
    {
        cout << "MotifButton\n";
    }
};

class MotifMenu : public Widget
{
public:
    void draw() override
    {
        cout << "MotifMenu\n";
    }
};

class WindowsButton : public Widget
{
public:
    void draw() override
    {
        cout << "WindowsButton\n";
    }
};

class WindowsMenu : public Widget {
public:
    void draw() override
    {
        cout << "WindowsMenu\n";
    }
};

class Window
{
    std::vector<std::unique_ptr<Widget>> widgets;
public:
    void display() const
    {
        std::cout << "######################\n";
        for(const auto& w : widgets)
            w->draw();
        std::cout << "######################\n\n";
    }

    void add_widget(std::unique_ptr<Widget> widget)
    {
        widgets.push_back(move(widget));
    }
};

class WindowOne : public Window
{

public:
    WindowOne()
    {
#ifdef MOTIF
        add_widget(std::unique_ptr<Widget>(new MotifButton));
        add_widget(std::unique_ptr<Widget>(new MotifMenu));
#else // WINDOWS
        add_widget(std::unique_ptr<Widget>(new WindowsButton));
        add_widget(std::unique_ptr<Widget>(new WindowsMenu));
#endif
    }
};

class WindowTwo : public Window
{

public:
    WindowTwo()
    {
#ifdef MOTIF
        add_widget(std::unique_ptr<Widget>(new MotifMenu));
        add_widget(std::unique_ptr<Widget>(new MotifButton));
        add_widget(std::unique_ptr<Widget>(new MotifButton));
#else // WINDOWS
        add_widget(std::unique_ptr<Widget>(new WindowsMenu));
        add_widget(std::unique_ptr<Widget>(new WindowsButton));
        add_widget(std::unique_ptr<Widget>(new WindowsButton));
#endif
    }
};


int main(void)
{
    WindowOne w1;
    w1.display();

    WindowTwo w2;
    w2.display();
}
