#include <iostream>
#include <vector>
#include <memory>
#include <cassert>
#include <fstream>

using namespace std;

struct Point
{
    int x = 0;
    int y = 0;

    Point(int x = 0, int y = 0) : x{x}, y{y}
    {}

    void translate(int dx, int dy)
    {
        x += dx;
        y += dy;
    }
};

ostream& operator<<(ostream& out, const Point& pt)
{
   out << "Point(x=" << pt.x << ", y=" << pt.y << ")";
   return out;
}

istream& operator>>(istream& in, Point& pt)
{
    char open_bracket, close_bracket, separator;

    in >> open_bracket >> pt.x >> separator >> pt.y >> close_bracket;

    return in;
}

class Shape
{
public:
    virtual ~Shape() = default;
    virtual void move(int x, int y) = 0;
    virtual void draw() const = 0;
};

class ShapeReaderWriter
{
public:
    virtual ~ShapeReaderWriter() = default;
    virtual void read(Shape& shp, istream& in) = 0;
    virtual void write(Shape& shp, ostream& out) = 0;
};

class ShapeBase : public Shape
{
    Point coord_; // composition
public:
    Point coord() const
    {
        return coord_;
    }

    void set_coord(const Point& pt)
    {
        coord_ = pt;
    }


    ShapeBase(int x = 0, int y = 0)
        : coord_{x, y}
    {}   

    void move(int dx, int dy) override
    {
        coord_.translate(dx, dy);
    }    
};

class Rectangle : public ShapeBase
{
    int width_, height_;
public:
    Rectangle(int x = 0, int y = 0, int w = 0, int h = 0)
        : ShapeBase{x, y}, width_{w}, height_{h}
    {}

    void set_width(int w)
    {
        width_ = w;
    }

    void set_height(int h)
    {
        height_ = h;
    }

    void draw() const override
    {
        cout << "Drawing rectangle at " << coord() << " with width: "  << width_
             << " and height: " << height_ << endl;
    }
};

class RectangleReaderWriter : public ShapeReaderWriter
{
    // ShapeReaderWriter interface
public:
    void read(Shape& shp, istream& in) override
    {
        Rectangle& rect = static_cast<Rectangle&>(shp);

        Point pt;
        int w, h;

        in >> pt >> w >> h;

        rect.set_coord(pt);
        rect.set_height(h);
        rect.set_width(w);
    }

    void write(Shape& shp, ostream& out) override
    {

    }
};

class Square : public Shape
{
    Rectangle rect_;
public:
    Square(int x = 0, int y = 0, int size = 0) : rect_{x, y, size, size}
    {
    }

    void set_coord(const Point& pt)
    {
        rect_.set_coord(pt);
    }

    void set_size(int size)
    {
        rect_.set_width(size);
        rect_.set_height(size);
    }

    void draw() const override
    {
        rect_.draw();
    }

    void move(int x, int y) override
    {
        rect_.move(x, y);
    }
};

class SquareReaderWriter : public ShapeReaderWriter
{

    // ShapeReaderWriter interface
public:
    void read(Shape& shp, istream& in) override
    {
        Square& sqr = static_cast<Square&>(shp);

        Point pt;
        int size;

        in >> pt >> size;

        sqr.set_size(size);
        sqr.set_coord(pt);
    }

    void write(Shape& shp, ostream& out) override
    {}
};

unique_ptr<Shape> create_shape(const string& id)
{
    if (id == "Rectangle")
        return make_unique<Rectangle>();
    else if (id == "Square")
        return make_unique<Square>();
}

unique_ptr<ShapeReaderWriter> create_shape_rw(const string&  id)
{
    if (id == "Rectangle")
        return make_unique<RectangleReaderWriter>();
    else if (id == "Square")
        return make_unique<SquareReaderWriter>();
}

class GraphicsDoc
{
    vector<unique_ptr<Shape>> shapes_;
public:
    void add(unique_ptr<Shape> shp)
    {
        shapes_.push_back(move(shp));
    }

    void render()
    {
        for(const auto& shp : shapes_)
            shp->draw();
    }

    void load(const string& filename)
    {
        ifstream file_in{filename};

        if (!file_in)
        {
            cout << "File not found!" << endl;
            exit(1);
        }

        while (file_in)
        {
            string shape_id;
            file_in >> shape_id;

            if (shape_id == "")
                return;

            cout << "Load " << shape_id << endl;

            auto shape = create_shape(shape_id);
            auto shape_rw = create_shape_rw(shape_id);

            shape_rw->read(*shape, file_in);

            shapes_.push_back(move(shape));
        }
    }

    void save(const string& filename)
    {
        //...
    }
};

GraphicsDoc create_doc()
{
    GraphicsDoc doc;

    unique_ptr<Rectangle> ptr_rect = make_unique<Rectangle>(10, 20, 100, 300);
    ptr_rect->draw();

    unique_ptr<Rectangle> ptr_other = move(ptr_rect);
    assert(ptr_rect == nullptr);

    doc.add(move(ptr_other));
    doc.add(make_unique<Square>(100, 500, 600));

    return doc;
}


int main(int argc, char *argv[])
{
    cout << "Start..." << endl;
    GraphicsDoc doc;
    doc.load("drawing.txt");

    doc.render();
}
