#include "shape_factories.hpp"
#include "circle.hpp"

using namespace std;

namespace
{
    bool is_registered = SingletonShapeFactory::instance().register_creator(
            Drawing::Circle::id, [] { return make_unique<Drawing::Circle>(); });
}

Drawing::Circle::Circle(int x, int y, int r) : ShapeBase{x, y}, radius_{r}
{}

void Drawing::Circle::draw() const
{
    std::cout << "Drawing a circle at " << coord() << " with radius: " << radius_ << std::endl;
}
