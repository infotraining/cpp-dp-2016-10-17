#include <iostream>
#include <vector>
#include <memory>
#include <cassert>
#include <fstream>
#include <unordered_map>

#include "shape.hpp"
#include "shape_factories.hpp"
#include "shape_group.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

class GraphicsDoc
{
    ShapeGroup shapes_;
    ShapeRWFactory& shape_rw_factory_;
public:
    GraphicsDoc(ShapeRWFactory& shape_rw_factory)
        : shape_rw_factory_{shape_rw_factory}
    {}

    void add(unique_ptr<Shape> shp)
    {
        shapes_.add(move(shp));
    }

    void render()
    {
        shapes_.draw();
    }

    void load(const string& filename)
    {
        ShapeGroup sg;

        ifstream file_in{filename};

        if (!file_in)
        {
            cout << "File not found!" << endl;
            exit(1);
        }

        string shape_id;
        file_in >> shape_id;

        assert(shape_id == ShapeGroup::id);

        auto shape_rw = shape_rw_factory_.create(shape_id);
        shape_rw->read(sg, file_in);

        shapes_ = move(sg);
    }

    void save(const string& filename)
    {
    }
};

int main()
{
    cout << "Start..." << endl;

    ShapeRWFactory shape_rw_factory;

    GraphicsDoc doc(SingtonShapeRWFactory::instance());

    doc.load("drawing.txt");

    cout << "\n";

    doc.render();

    cout << "\n";

    GraphicsDoc doc2 = doc;

    doc2.render();
}
