#include "point.hpp"

std::ostream&operator<<(std::ostream& out, const Drawing::Point& pt)
{
    out << "Point(x=" << pt.x << ", y=" << pt.y << ")";
    return out;
}

std::istream&operator>>(std::istream& in, Drawing::Point& pt)
{
    char open_bracket, close_bracket, separator;

    in >> open_bracket >> pt.x >> separator >> pt.y >> close_bracket;

    return in;
}
