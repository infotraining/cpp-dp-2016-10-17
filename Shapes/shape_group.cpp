#include <algorithm>
#include "shape_group.hpp"
#include "shape_factories.hpp"

namespace
{
    bool is_registered = SingletonShapeFactory::instance().register_creator(
                Drawing::ShapeGroup::id, &std::make_unique<Drawing::ShapeGroup>);
}


Drawing::ShapeGroup::ShapeGroup(const Drawing::ShapeGroup& source)
{
    std::transform(source.shapes_.begin(), source.shapes_.end(),
                   std::back_inserter(shapes_),
                   [](const auto& shp) { return shp->clone();});
}

Drawing::ShapeGroup&Drawing::ShapeGroup::operator=(const Drawing::ShapeGroup& source)
{
    ShapeGroup temp(source);
    swap(temp);

    return *this;
}

void Drawing::ShapeGroup::add(std::unique_ptr<Drawing::Shape> shp)
{
    shapes_.push_back(std::move(shp));
}

void Drawing::ShapeGroup::move(int x, int y)
{
    for(const auto& shp : shapes_)
        shp->move(x, y);
}

void Drawing::ShapeGroup::draw() const
{
    for(const auto& shp : shapes_)
        shp->draw();
}

std::unique_ptr<Drawing::Shape> Drawing::ShapeGroup::clone() const
{
    return std::make_unique<ShapeGroup>(*this);
}

void Drawing::ShapeGroup::swap(Drawing::ShapeGroup& other)
{
    shapes_.swap(other.shapes_);
}
