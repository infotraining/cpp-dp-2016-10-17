#ifndef SHAPEGROUP_HPP
#define SHAPEGROUP_HPP

#include <vector>

#include "shape.hpp"

namespace Drawing
{
    class ShapeGroup : public Shape
    {
        std::vector<std::unique_ptr<Shape>> shapes_;

        void swap(ShapeGroup& other);

    public:
        static constexpr const char* id = "ShapeGroup";

        ShapeGroup() = default;

        ShapeGroup(const ShapeGroup& source);

        ShapeGroup& operator=(const ShapeGroup& source);

        ShapeGroup(ShapeGroup&&) noexcept = default;

        ShapeGroup& operator=(ShapeGroup&&) noexcept = default;

        void add(std::unique_ptr<Shape> shp);

        void move(int x, int y) override;

        void draw() const override;

        std::unique_ptr<Shape> clone() const override;
    };

}


#endif // SHAPEGROUP_HPP
