#include "circle_reader_writer.hpp"
#include "../shape_factories.hpp"

using namespace std;

namespace
{
    bool is_registered = SingtonShapeRWFactory::instance().register_creator(
                Drawing::Circle::id, make_unique<Drawing::IO::CircleReaderWriter>);
}


void Drawing::IO::CircleReaderWriter::read(Drawing::Shape& shp, std::istream& in)
{
    Point pt;
    int r;

    in >> pt >> r;

    Circle& c = static_cast<Circle&>(shp);
    c.set_coord(pt);
    c.set_radius(r);
}
