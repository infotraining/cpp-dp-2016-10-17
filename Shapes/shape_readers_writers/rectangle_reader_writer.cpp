#include "rectangle_reader_writer.hpp"
#include "../shape_factories.hpp"

using namespace std;

namespace
{
    bool is_registered = SingtonShapeRWFactory::instance().register_creator(
                Drawing::Rectangle::id,
                []{ return make_unique<Drawing::IO::RectangleReaderWriter>(); });
}

void Drawing::IO::RectangleReaderWriter::read(Drawing::Shape& shp, std::istream& in)
{
    Rectangle& rect = static_cast<Rectangle&>(shp);

    Point pt;
    int w, h;

    in >> pt >> w >> h;

    rect.set_coord(pt);
    rect.set_height(h);
    rect.set_width(w);
}

void Drawing::IO::RectangleReaderWriter::write(Drawing::Shape& shp, std::ostream& out)
{
}
