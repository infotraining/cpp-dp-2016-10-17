#include "shape_group_reader_writer.hpp"

namespace
{
    bool is_registered = SingtonShapeRWFactory::instance().register_creator(
                Drawing::ShapeGroup::id,
                []{ return std::make_unique<Drawing::IO::ShapeGroupReaderWriter>(SingletonShapeFactory::instance(), SingtonShapeRWFactory::instance()); });
}

void Drawing::IO::ShapeGroupReaderWriter::read(Drawing::Shape& shp, std::istream& in)
{
    ShapeGroup& shape_group = static_cast<ShapeGroup&>(shp);

    int size;

    in >> size;

    for(int i = 0; i < size; ++i)
    {
        std::string shape_id;
        in >> shape_id;
        std::cout << "Loading in ShapeGroup: " << shape_id << "..." << std::endl;

        auto shape = shape_factory_.create(shape_id);
        auto shape_rw = shape_rw_factory_.create(shape_id);

        shape_rw->read(*shape, in);


        shape_group.add(move(shape));
    }
}
