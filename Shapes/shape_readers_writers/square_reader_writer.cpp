#include "../shape_factories.hpp"
#include "square_reader_writer.hpp"

using namespace std;

namespace
{
    bool is_registered = SingtonShapeRWFactory::instance().register_creator(
                Drawing::Square::id,
                []{ return make_unique<Drawing::IO::SquareReaderWriter>(); });
}

void Drawing::IO::SquareReaderWriter::read(Drawing::Shape& shp, std::istream& in)
{
    Square& sqr = static_cast<Square&>(shp);

    Point pt;
    int size;

    in >> pt >> size;

    sqr.set_size(size);
    sqr.set_coord(pt);
}

void Drawing::IO::SquareReaderWriter::write(Drawing::Shape& shp, std::ostream& out)
{}
