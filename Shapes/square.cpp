#include <memory>
#include "square.hpp"
#include "shape_factories.hpp"

using namespace std;

namespace
{
    bool is_registered = SingletonShapeFactory::instance().register_creator(
                Drawing::Square::id, &make_unique<Drawing::Square>);
}


Drawing::Square::Square(int x, int y, int size) : rect_{x, y, size, size}
{
}

void Drawing::Square::move(int x, int y)
{
    rect_.move(x, y);
}

Drawing::Point Drawing::Square::coord() const
{
    return rect_.coord();
}

void Drawing::Square::set_coord(const Drawing::Point& pt)
{
    rect_.set_coord(pt);
}

void Drawing::Square::set_size(int size)
{
    rect_.set_width(size);
    rect_.set_height(size);
}

void Drawing::Square::draw() const
{
    rect_.draw();
}
