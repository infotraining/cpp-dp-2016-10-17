#include "text.hpp"
#include "shape_factories.hpp"

namespace
{
    bool is_registered = SingletonShapeFactory::instance().register_creator(
                Drawing::Text::id, &std::make_unique<Drawing::Text>);
}

Drawing::Text::Text(int x, int y, const std::string& text)
    : ShapeBase{x, y}, LegacyCode::Paragraph{text.c_str()}
{}

void Drawing::Text::draw() const
{
    render_at(coord().x, coord().y);
}

std::unique_ptr<Drawing::Shape> Drawing::Text::clone() const
{
    return std::make_unique<Text>(*this);
}
