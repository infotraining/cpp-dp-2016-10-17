#ifndef TEXT_HPP
#define TEXT_HPP

#include "shape.hpp"
#include "paragraph.hpp"

namespace Drawing
{
    class Text : public ShapeBase, private LegacyCode::Paragraph
    {
    public:
        static constexpr const char* id = "Text";

        Text(int x = 0, int y = 0, const std::string& text = "");

        std::string text() const
        {
            return get_paragraph();
        }

        void set_text(const std::string& text)
        {
            set_paragraph(text.c_str());
        }

        void draw() const override;

        std::unique_ptr<Shape> clone() const override;
    };
}

#endif // TEXT_HPP
